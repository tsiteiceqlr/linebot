
// logger simple-node-logger
const SimpleNodeLogger = require('simple-node-logger');

class Logger {
    constructor() {
        this.init = this.init.bind(this);
        this.log = this.log.bind(this);
        this.error = this.error.bind(this);
    }
    
    init(fileName){
        this.fileName = fileName;
        const opts1 = {
            logDirectory: './logs',
            fileNamePattern: `${this.fileName}-<DATE>.log`,
            dateFormat: 'YYYY-MM-DD',
            timestampFormat: 'YYYY-MM-DD HH:mm:ss.SSS'
        };
        this.normalLogger = SimpleNodeLogger.createRollingFileLogger(opts1);

        const opts2 = {
            logDirectory: './logs',
            fileNamePattern: `${this.fileName}-error-<DATE>.log`,
            dateFormat: 'YYYY-MM-DD',
            timestampFormat: 'YYYY-MM-DD HH:mm:ss.SSS'
        };
        this.errorLogger = SimpleNodeLogger.createRollingFileLogger(opts2);
    };
    log(){
        let mylog = `${Object.values(arguments).join(' ')}`;

        this.normalLogger.info(mylog);
    };
    error() {
        let mylog = `${Object.values(arguments).join(' ')}`;

        this.errorLogger.error(mylog);
    };
}

module.exports = new Logger();