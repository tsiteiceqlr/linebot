var express = require('express');
var router = express.Router();
const configs = require('../configs.js');
const channelAccessToken = configs.channelAccessToken;
const line = require('@line/bot-sdk');

const AssistantCtl = require('../Controller/AssistantController');
const MongodbCtl = require('../Controller/MongodbController');
const Logger = require('../Controller/Logger');

const client = new line.Client({
  channelAccessToken: channelAccessToken
});

// logger simple-node-logger
// const SimpleNodeLogger = require('simple-node-logger');

Logger.init('webhook');
MongodbCtl.init();


// const Mclient = new MongoClient(uri, { useNewUrlParser: true, useUnifiedTopology: true});
// Mclient.connect((err, db) => {

//   if (!err) console.log('connect to db')

//   var dbo = db.db("runoob");
//   var myobj = { name: "菜鸟教程", url: "www.runoob" };
//   dbo.collection("site").insertOne(myobj, function(err, res) {
//     if (err) throw err;
//     console.log("文档插入成功");
//     db.close();
//   });

//   const collection = Mclient.db("test").collection("devices");
//   // perform actions on the collection object
//   Mclient.close();
// });


/* line webhook */
router.post('/line', async function(req, res, next) {

  // 印出傳入的資料
  logger.log('webhook received an event');

  // res 200 表示正常接收
  res.status(200).send();

  let targetEvent = req.body.events[0];
  if (targetEvent.type == 'message') {
    if (targetEvent.message.type == 'text') {
      let userSay = targetEvent.message.text;
      let userId = targetEvent.source.userId;
      logger.log('userSay:', userSay);
      logger.log('userSay:', userId);

      // 取得回覆
			let AssistantReturn = await AssistantCtl.sendMessage(userId, userSay);
      let returnMessage = {
				type: 'text',
				text: AssistantReturn.output.generic[0].text
			};
			
			// 將回覆傳回給user
      replyToLine(targetEvent.replyToken, returnMessage);      
			Logger.log(userId, 'return message:', returnMessage);

      // 紀錄DB
			let document = {
				timestamp: Date.now(),
				userId: userId,
				userSay: userSay,
				AssistantReturn: AssistantReturn
      }
      MongodbCtl.insertOne('LineBot', 'chatLog', document);
    }    
  }
});

module.exports = router;

function replyToLine(rplyToken, message) {  
  return client.replyMessage(rplyToken, message)
	.then((res) => {
		// console.log(res)
		return true; 
	})
	.catch((err) => {
		// console.log(err)
		return false;
	});
}


// let logger = {
// 	init: function(fileName){
// 		const opts1 = {
// 			logDirectory: './logs',
// 			fileNamePattern: `${fileName}-<DATE>.log`,
// 			dateFormat: 'YYYY-MM-DD',
// 			timestampFormat: 'YYYY-MM-DD HH:mm:ss.SSS'
// 		};
// 		this.normalLogger = SimpleNodeLogger.createRollingFileLogger(opts1);

// 		const opts2 = {
// 			logDirectory: './logs',
// 			fileNamePattern: `${fileName}-error-<DATE>.log`,
// 			dateFormat: 'YYYY-MM-DD',
// 			timestampFormat: 'YYYY-MM-DD HH:mm:ss.SSS'
// 		};
// 		this.errorLogger = SimpleNodeLogger.createRollingFileLogger(opts2);
// 	},
// 	log: function(){
// 		let mylog = `${Object.values(arguments).join(' ')}`;

// 		this.normalLogger.info(mylog);
// 	},
// 	error: function(){
// 		let mylog = `${Object.values(arguments).join(' ')}`;

// 		this.errorLogger.error(mylog);
// 	}
// }
// logger.init('webhook');